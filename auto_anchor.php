<?php
/*
Plugin Name: Auto Anchor
Description: Generates anchors for h[1-4] headings
Author: Rishabh Moudgil
Version: 0.1
*/

class Auto_Anchor {
	static function replace_headings($content) {
		if (is_singular() && is_main_query()) {
			$dom = new DOMDocument();
			$dom->loadHTML($content);
			$headings = array('h1', 'h2', 'h3', 'h4');

			foreach ($headings as $heading) {

				$elements = $dom->getElementsByTagName($heading);
				foreach ($elements as $element) {
					$slug = sanitize_title($element->textContent);
					$a = $dom->createElement('a');
					$a->setAttribute('id', $slug);
					$a->setAttribute('href', "#" . $slug);
					$ref = $element->parentNode->insertBefore($a, $element);
					$ref->appendChild($element);
				}
			}
			return $dom->saveHTML();
		} else {
			return $content;
		}
	}
}

add_filter('the_content', array('Auto_Anchor', 'replace_headings'), 999);

?>
